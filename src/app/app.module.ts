import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AddEditClientComponent } from './client/add-edit-client/add-edit-client.component';
import { ListingClientComponent } from './client/listing-client/listing-client.component';
import { AddEditProjectComponent } from './project/add-edit-project/add-edit-project.component';
import { ListingProjectComponent } from './project/listing-project/listing-project.component';
import { LogInComponent } from './log-in-component/log-in.component';

import { AuthModule } from './auth/auth.module';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AddEditClientComponent,
    ListingClientComponent,
    AddEditProjectComponent,
    ListingProjectComponent,
    LogInComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule,
    AuthModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
