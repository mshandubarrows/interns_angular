import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { AddEditClientComponent } from './client/add-edit-client/add-edit-client.component';
import { ListingClientComponent } from './client/listing-client/listing-client.component';
import { AddEditProjectComponent } from './project/add-edit-project/add-edit-project.component';
import { ListingProjectComponent } from './project/listing-project/listing-project.component';
import { LogInComponent } from './log-in-component/log-in.component';

//import {AuthGuard} from './auth.guard';

const routes: Routes = [
{path:'',redirectTo:'/log-in', pathMatch:'full'},
{path:'log-in',component:LogInComponent},

{path:'home',component:HeaderComponent,
children:[
   {path:'add-client',component:AddEditClientComponent},
   {path:'edit-client/:id',component:AddEditClientComponent},
   {path:'listing-client',component:ListingClientComponent},
  
   {path:'add-project' ,component:AddEditProjectComponent},
   {path:'edit-project/:id' ,component:AddEditProjectComponent},
   {path:'listing-project' ,component:ListingProjectComponent}
]}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { }
