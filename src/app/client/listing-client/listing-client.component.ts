import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class ClientListing {
  constructor(
    public id: number,
    public client_name: string,
    public client_address: string
  ) {
  }
}
@Component({
  selector: 'app-listing-client',
  templateUrl: './listing-client.component.html',
  styleUrls: ['./listing-client.component.scss']
})
export class ListingClientComponent implements OnInit {

  listing: ClientListing[];
  headElements = ['Client ID', 'Client Name', 'Client Address', 'Edit/Delete'];
  status;
  errorMessage;

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.getClientListing();
  }

  getClientListing(){
    this.httpClient.get<any>('https://barrows-api-assessment.herokuapp.com/clients').subscribe(
      response => {
       // console.log(response);
        this.listing = response;
      });
  }
  
  deleteClient(id:string){
    this.httpClient.delete('https://barrows-api-assessment.herokuapp.com/clients/'+ id)
    .subscribe({next: data => {this.status = 'Delete successful'},
                error: error =>{this.errorMessage = error.message;
                                console.error('There was an error!', error);}

                });
  }
}
