import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-add-edit-client',
  templateUrl: './add-edit-client.component.html',
  styleUrls: ['./add-edit-client.component.scss']
})
export class AddEditClientComponent implements OnInit {
  clientForm:FormGroup
  isAddMode : boolean;
  id: string;

  constructor(private formBuilder:FormBuilder,private httpClient:HttpClient,
              private route:ActivatedRoute, private router: Router) {
    this.clientForm = formBuilder.group({
      client_name: ['', Validators.required],
      client_address: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;

    if(!this.isAddMode){
      this.getClient();
    }

  }

  addClient(){
    
    this.httpClient.post('https://barrows-api-assessment.herokuapp.com/clients', this.clientForm.value)
    .subscribe(result => {console.warn("result",result)
    });
    //console.log("new client added ");
    this.clientForm.reset();
  }

  getClient(){
    this.httpClient.get<any>('https://barrows-api-assessment.herokuapp.com/clients/' + this.id)
    .pipe(first())
    .subscribe(x => this.clientForm.patchValue(x));
  }

  saveClient(){

    if(this.isAddMode){
      this.addClient();
    }
    else{
      this.updateClient();
    }

  }
  
  updateClient(){
    this.httpClient.put<any>('https://barrows-api-assessment.herokuapp.com/clients/' + this.id, this.clientForm.value)
    .pipe(first())
    .subscribe(() => {this.router.navigate(['home']);})
  }

  get clientName() { return this.clientForm.get('client_name'); }

  get clientAddress() { return this.clientForm.get('client_address'); }


}
