import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

export class ProjectListing {
  constructor(
    public id: number,
    public client_id: number,
    public project_desc: string
  ) {
  }
}
@Component({
  selector: 'app-listing-project',
  templateUrl: './listing-project.component.html',
  styleUrls: ['./listing-project.component.scss']
})


export class ListingProjectComponent implements OnInit {

  listing: ProjectListing[];
  headElements = ['Project ID', 'Client ID', 'Project Description', 'Edit/Delete'];
  status;
  errorMessage;

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.getProjectListing();
  }

  getProjectListing(){
    this.httpClient.get<any>('https://barrows-api-assessment.herokuapp.com/projects').subscribe(
      response => {
       // console.log(response);
        this.listing = response;
      }
    );
  }
  
  deleteProject(id:string){
    this.httpClient.delete('https://barrows-api-assessment.herokuapp.com/projects/'+ id)
    .subscribe({next: data => {this.status = 'Delete successful'},
                error: error =>{this.errorMessage = error.message;
                                console.error('There was an error!', error);}

                });
  }
  
}
