import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

export class ClientsList {
  constructor(
    public id: number,
    public client_name: string,
  ) {
  }
}
@Component({
  selector: 'app-add-edit-project',
  templateUrl: './add-edit-project.component.html',
  styleUrls: ['./add-edit-project.component.scss']
})
export class AddEditProjectComponent implements OnInit {

  projectForm: FormGroup;
  listing: ClientsList[];
  project_desc: string;
  id:number;
  isAddMode:boolean;

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient,  
              private route:ActivatedRoute, private router: Router) { 
    
    this.projectForm = formBuilder.group({
      client_id:['', Validators.required],
      project_desc:['', Validators.required]});

  }

  ngOnInit(): void{
    this.getClientListing();
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;

    if(!this.isAddMode){
      this.getProject();
    }
  }

  addProject(){
    //console.log("new project added ");
    this.httpClient.post('https://barrows-api-assessment.herokuapp.com/projects', this.projectForm.value)
    .subscribe(result => {console.warn("result",result)
    });
    this.projectForm.reset();
  }

  getClient() {
    return this.listing;
  }

  getClientListing(){
    this.httpClient.get<any>('https://barrows-api-assessment.herokuapp.com/clients').subscribe(
      response => {
        //console.log(response);
        this.listing = response;
      }
    );
  }

  getProject(){
    this.httpClient.get<any>('https://barrows-api-assessment.herokuapp.com/projects/' + this.id)
    .pipe(first())
    .subscribe(x => this.projectForm.patchValue(x));
  }

  saveProject(){

    if(this.isAddMode){
      this.addProject();
    }
    else{
      this.updateProject();
    }

  }

  updateProject(){
    this.httpClient.put<any>('https://barrows-api-assessment.herokuapp.com/projects/' + this.id, this.projectForm.value)
    .pipe(first())
    .subscribe(() => {this.router.navigate(['home']);}) 
  }

  get projectDescription() { return this.projectForm.get('project_desc'); }
}
