import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  loginForm :FormGroup;

  constructor(private fb:FormBuilder, private authService: AuthService, private router: Router) {
   }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['',Validators.required],
      password: ['',Validators.required]
    });
  }

  get logins() { return this.loginForm.controls; }

  login() {
    this.authService.login(
      {
        username: this.logins.username.value,
        password: this.logins.password.value
      }
    )
    .subscribe(success => {
      if (success) {
        this.router.navigate(['/home']);
      }
    });
  }

  formIsValid(): boolean{
    return this.loginForm.valid;
}

}
