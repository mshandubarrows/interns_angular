import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import * as moment from "moment";
import { Observable, of } from 'rxjs';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { Tokens } from './tokens';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly JWT_TOKEN: string = 'JWT_TOKEN';
  private readonly REFRESH_TOKEN: string = 'REFRESH_TOKEN';
  private loggedUser: string;

  constructor(private http: HttpClient) { }

  login(user: { username:string, password:string }): Observable<boolean>{
    return this.http.post<any>(`https://barrows-api-assessment.herokuapp.com/api/token`, user)
      .pipe(
        tap(tokens => this.doLoginUser(user.username, tokens)),
        mapTo(true),
        catchError(error => {
          alert(error.error);
          return of(false);
        })
      );
  }
/*
  logout() {
    return this.http.post<any>(`https://barrows-api-assessment.herokuapp.com/api/token/refresh`, {
      'refreshToken': this.getRefreshToken()
    }).pipe(
      tap(() => this.doLogoutUser()),
      mapTo(true),
      catchError(error => {
        alert(error.error);
        return of(false);
      }));
  }*/

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  refreshToken() {
    return this.http.post<any>(`https://barrows-api-assessment.herokuapp.com/api/token/refresh`, {
      'refreshToken': this.getRefreshToken()
    }).pipe(tap((tokens: Tokens) => {
      this.storeJwtToken(tokens.jwt);
    }));
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  private doLoginUser(username: string, tokens: Tokens) {
    this.loggedUser = username;
    this.storeTokens(tokens);
  }
/*
  private doLogoutUser() {
    this.loggedUser = null;
    this.removeTokens();
  }*/

  private getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  private storeTokens(tokens: Tokens) {
    var tokenStrings = JSON.stringify(tokens).split(",",2);
    tokens.jwt = tokenStrings[1].substr(10,207);
    tokens.refreshToken = tokenStrings[0].substr(12,207);
    
    localStorage.setItem(this.JWT_TOKEN, tokens.jwt);
    localStorage.setItem(this.REFRESH_TOKEN, tokens.refreshToken);
    console.log(tokens.jwt);
    console.log(tokens.refreshToken);
  }
/*
  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
  }*/
    
}
